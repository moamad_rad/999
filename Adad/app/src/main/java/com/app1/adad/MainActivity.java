package com.app1.adad;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Array;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn1 = (Button) findViewById(R.id.btn1);
        final EditText editText = (EditText) findViewById(R.id.edittext1);
        final String[] yekan = {"صفر", "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه"};
        final String[] estesna = {"ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هیجده", "نوزده", "بیست"};
        final String[] dahgan = {"", "", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"};
        final String[] sadgan = {"", "صد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"};
        final String[] va = {"", " و "};
        btn1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        String curent = editText.getText().toString();
                                        int inputnum = Integer.parseInt(curent);
                                        int firstpart = inputnum % 10;
                                        int secondpart = inputnum / 10;
                                        int thirdpart = secondpart % 10;
                                        int fourthpart = secondpart / 10;
                                        if (thirdpart == 1 && fourthpart != 0) {
                                            Toast.makeText(MainActivity.this, sadgan[fourthpart] + va[1] + estesna[firstpart], Toast.LENGTH_SHORT).show();
                                        } else if (fourthpart == 0 && thirdpart == 0) {
                                            Toast.makeText(MainActivity.this, yekan[firstpart], Toast.LENGTH_SHORT).show();
                                        } else if (firstpart == 0 && thirdpart == 0) {
                                            Toast.makeText(MainActivity.this, sadgan[fourthpart], Toast.LENGTH_SHORT).show();
                                        } else if (firstpart == 0 && fourthpart == 0) {
                                            Toast.makeText(MainActivity.this, dahgan[thirdpart], Toast.LENGTH_SHORT).show();
                                        } else if (firstpart == 0) {
                                            Toast.makeText(MainActivity.this, sadgan[fourthpart] + va[1] + dahgan[thirdpart], Toast.LENGTH_SHORT).show();

                                        } else if (thirdpart == 0) {
                                            Toast.makeText(MainActivity.this, sadgan[fourthpart] + va[1] + yekan[firstpart], Toast.LENGTH_SHORT).show();
                                        } else if (fourthpart == 0) {
                                            if (thirdpart == 1) {
                                                Toast.makeText(MainActivity.this, estesna[firstpart], Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(MainActivity.this, dahgan[thirdpart] + va[1] + yekan[firstpart], Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(MainActivity.this, sadgan[fourthpart] + va[1] + dahgan[thirdpart] + va[1] + yekan[firstpart], Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
        );
    }
}
